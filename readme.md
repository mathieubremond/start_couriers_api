# Senior Solutions Engineer test for Stuart

Hello, my name is Mathieu.

I will walk you through the code I wrote as part of my recruitement process for Stuart.

The goal is to develop a small API for Couriers. They all have a specific capacity, and based on their current one, a Dispatcher needs to lookup if couriers are available for delivery.

I tried to keep this project low on dependencies. Even if frameworks and libraries are great tools, I truly believe most of them are too powerful for the usual tasks at hand.

## Coding patterns and Object Oriented Programming

I didn't push the patterns I am used to as far as possible during this tests.  
Keep in mind that I come from a Java background (before switcing to JS), therefore I like to use OOP when it makes sense.  
I teached IT students about Java and OOP both during my studies and profesionnaly (one time).

## Requirements

You need `NodeJS` on your machine to run this API.
Please run `npm install` inside the project before running the tests or the API.

## Running the tests

I used Mocha and Chai for my tests. Run `npm test` to start them all.
The database used for the test is a diferent one from the production environment. Both databases are stored in the cloud for this project.

You can also test with Postman. I have saved a collection here : https://www.getpostman.com/collections/9db50e1497eb72187ba7

## Running the API

To launch the API, simply run `npm start`.

The Mongo Database I used is stored on a free cluster in Mongo Atlas, so don't worry about populating a database.
If needed, json files with sample data are available in the `src/tests/data` folder.

### Additional note

I did this test in approximatively 4 hours.

Repository : https://gitlab.com/mathieubremond/start_couriers_api
