// Being in a test env make the tests use the test database (stored in /config)
process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const app = require('../server');
const Courier = require('../models/courier');
const exampleDataSet = require('./data/couriers.json');
chai.use(chaiHttp);

describe('Couriers API', () => {
    let defaultCourierId = null;

    beforeEach(done => {
        // We clean the database before each tests
        Courier.deleteMany({}, err => {
            Courier.insertMany(exampleDataSet, err => {
                Courier.findOne({}, (err, res) => {
                    defaultCourierId = res._id;
                    console.log('defaultCourierId = ', defaultCourierId);
                    done();
                });
            });
        });
    });

    describe('POST /couriers', () => {
        it('it should insert a new courier', done => {
            const courier = { maxCapacity: 999 };
            chai.request(app)
                .post('/couriers')
                .send(courier)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body.courier).to.be.an('object');
                    expect(res.body.courier).to.have.property('_id');
                    expect(res.body.courier).to.have.property('maxCapacity');
                    expect(res.body.courier).to.have.property(
                        'availableCapacity'
                    );
                    expect(res.body.courier).to.have.property('createdOn');
                    done();
                });
        });
    });

    describe('GET /couriers', () => {
        it('it should get all the couriers', done => {
            chai.request(app)
                .get('/couriers')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('array');
                    expect(res.body.length).to.be.equal(5);
                    done();
                });
        });
    });

    describe('POST /couriers/lookup', () => {
        it('it should insert five new couriers and then lookup for the ones with a capacity greater than 100L', done => {
            const lookupArgs = { capacityRequired: 100 };

            const res = chai
                .request(app)
                .post('/couriers/lookup')
                .send(lookupArgs)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('array');
                    expect(res.body.length).to.be.equal(2);

                    done();
                });
        });
    });

    describe('DELETE /couriers/:id', () => {
        it('it should delete one courier', done => {
            const url = `/couriers/${defaultCourierId}`;

            const res = chai
                .request(app)
                .delete(url)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    done();
                });
        });
    });

    describe('PUT /couriers/:id/capacity', () => {
        it('it should update a courier with a new available capacity of 50L', done => {
            const postArgs = { availableCapacity: 50 };
            const url = `/couriers/${defaultCourierId}/capacity`;

            console.log('url = ', url);

            const res = chai
                .request(app)
                .put(url)
                .send(postArgs)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.an('object');
                    expect(res.body.courier).to.be.an('object');
                    expect(res.body.courier).to.have.property(
                        'availableCapacity'
                    );
                    expect(res.body.courier.availableCapacity).to.be.equal(50);

                    done();
                });
        });
    });
});
