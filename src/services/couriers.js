const Courier = require('../models/courier');

class CourierService {
    async findOne({ id }) {
        return await Courier.findById(id);
    }

    async findAll() {
        return await Courier.find();
    }

    async lookupCapacities({ capacityRequired }) {
        return await Courier.find({
            availableCapacity: { $gte: capacityRequired },
        });
    }

    async create({ maxCapacity }) {
        if (maxCapacity < 0)
            throw new Error('Max capacity must be a positive number');
        const courier = new Courier({ maxCapacity });
        await courier.save();
        return courier._doc;
    }

    async update() {
        throw new Error('Not implemented yet');
    }

    async updateCapacity({ id, availableCapacity }) {
        const courier = await this.findOne({ id });

        if (!courier) throw new Error('Courier not found');

        if (courier.maxCapacity < availableCapacity)
            throw new Error(
                "Courier's available capacity can not be superior to his maximum capacity"
            );

        await Courier.updateOne(
            { _id: courier._id },
            { $set: { availableCapacity } }
        );

        return await this.findOne({ id });
    }

    async deleteOne({ id }) {
        const courier = await Courier.findById(id);

        if (!courier) throw new Error('Courier not found');

        await Courier.deleteOne({ _id: courier._id });

        return true;
    }
}

module.exports = CourierService;
