const express = require('express');
const router = express.Router();
const Courier = require('../models/courier');
const CourierService = require('../services/couriers');
const { check, validationResult } = require('express-validator');

// @route   GET /couriers
// @desc    get all couriers
// @access  public
router.get('/', async (req, res) => {
    try {
        const couriers = await new CourierService().findAll();
        return res.json(couriers);
    } catch (error) {
        console.error(error.message);
        return res.status(500).send({
            errors: [
                {
                    msg: error.message,
                },
            ],
        });
    }
});

// @route   POST /couriers/lookup
// @desc    get the couriers with an available capacity greater or equal to the one required
// @access  public
router.post(
    '/lookup',
    [
        check('capacityRequired', 'A capacity is required')
            .not()
            .isEmpty()
            .isNumeric(),
    ],
    async (req, res) => {
        const { capacityRequired } = req.body;
        try {
            const couriers = await new CourierService().lookupCapacities({
                capacityRequired,
            });
            return res.json(couriers);
        } catch (error) {
            console.error(error.message);
            return res.status(500).send({
                errors: [
                    {
                        msg: error.message,
                    },
                ],
            });
        }
    }
);

// @route   POST /couriers
// @desc    create a courier
// @access  public
router.post(
    '/',
    [
        check('maxCapacity', "A maximum courier's capacity is required")
            .not()
            .isEmpty()
            .isNumeric(),
    ],
    async (req, res) => {
        // We look for potential errors in the request
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { maxCapacity } = req.body;

        try {
            const courier = await new CourierService().create({ maxCapacity });
            return res.json({ courier });
        } catch (error) {
            console.error(error.message);
            return res.status(500).send({
                errors: [
                    {
                        msg: error.message,
                    },
                ],
            });
        }
    }
);

// @route   PUT /couriers
// @desc    update a courier's capacity
// @access  public
router.put(
    '/:id/capacity',
    [
        check(
            'availableCapacity',
            'A current available capacity must be provided'
        )
            .not()
            .isEmpty()
            .isNumeric(),
    ],
    async (req, res) => {
        // We look for potential errors in the request
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const id = req.params.id;
        const { availableCapacity } = req.body;
        try {
            const courier = await new CourierService().updateCapacity({
                id,
                availableCapacity,
            });
            return res.json({ courier });
        } catch (error) {
            console.error(error.message);
            return res.status(500).send({
                errors: [
                    {
                        msg: error.message,
                    },
                ],
            });
        }
    }
);

// @route   delete /couriers
// @desc    delete a courier
// @access  public
router.delete('/:id', async (req, res) => {
    const id = req.params.id;
    try {
        await new CourierService().deleteOne({ id });
        return res.json({});
    } catch (error) {
        console.error(error.message);
        return res.status(500).send({
            errors: [
                {
                    msg: error.message,
                },
            ],
        });
    }
});

module.exports = router;
