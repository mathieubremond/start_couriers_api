const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');

const connectDatabase = async () => {
    try {
        await mongoose.connect(db, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
        });
    } catch (e) {
        console.error(e.message);
        process.exit(1);
    }
};

module.exports = connectDatabase;
