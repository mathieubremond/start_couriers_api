const mongoose = require('mongoose');
const courierSchema = new mongoose.Schema({
    /* The maximum capacity a courier has in liters */
    maxCapacity: { type: Number, required: true },

    /* The current capacity available in liters */
    availableCapacity: {
        type: Number,
        default: 0 /* default value set inside pre save hook */,
        required: true,
    },

    /* The creation date of this courier */
    createdOn: {
        type: Date,
        default: Date.now,
        required: true,
    },
});

courierSchema.pre('save', function (next) {
    // When created, the courier has a maximum available capacity
    this.availableCapacity = this.get('maxCapacity');
    next();
});

module.exports = Courier = mongoose.model('courier', courierSchema);
