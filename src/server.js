const PORT = process.env.PORT || 8000;

const express = require('express');
const connectDatabase = require('./database/connect');

const app = express();
app.use(express.json({ extended: false }));

// Connect to the mongo db
connectDatabase();

// Expose API
app.use('/couriers', require('./api/couriers'));

app.get('/', (req, res) => res.status(200).send());

app.listen(PORT, () => {
    console.log(
        `[STUART COURIER CAPACITIES API] Listening on: http://localhost:${PORT}`
    );
});

module.exports = app;
